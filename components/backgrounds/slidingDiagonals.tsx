import { ReactNode } from "react";

export default function SlidingDiagonals({
  children,
  className
}: {
  children: ReactNode;
  className: string;
}) {
  return (
    <>
      <div className={className} style={{position: 'relative'}}>
        <div className="bg"></div>
        <div className="bg bg2"></div>
        <div className="bg bg3"></div>
        <div className="contentSlidingDiagonalsBg">{children}</div>
      </div>
    </>
  );
}
