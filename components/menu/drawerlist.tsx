import { BrowseGallery, DesignServices, Home, Hotel,
    Pages } from "@mui/icons-material";
    import {
      List,
      ListItem,
      ListItemButton,
      ListItemIcon,
      ListItemText,
    } from "@mui/material";
    import { Box } from "@mui/system";
    
    export default function DrawerList() {
      const links = [{
        href:"/",
        text: 'home',
        icone: <Home />
      }, {
        href: "/landingpages/landing-imobiliaria",
        text: 'Viagem/Hotel',
        icone: <Hotel />
      },{
        href: '/landingpages/landing-designer',
        text: 'Agencia Design',
        icone: <DesignServices />
      },{
        href: '/landingpages/promocaoproduto',
        text: 'Promoção Produto (shopify)',
        icone: <BrowseGallery />
      },
      {
        href: '/landingpages/blogposts',
        text:'Blog',
        icone: <Pages/>
      }
    ];
      return (
        <Box
          sx={{
            maxWidth: 280,
          }}
          role="presentation"
        >
          <div className="w-[280px]">
            <List>
              {links.map((item, index) => (
                <ListItem disablePadding key={index}>
                  <ListItemButton component="a" href={item.href}>
                    <ListItemIcon>
                      {item.icone}
                    </ListItemIcon>
                    <ListItemText>{item.text}</ListItemText>
                  </ListItemButton>
                </ListItem>
              ))}
            </List>
          </div>
        </Box>
      );
    }
    