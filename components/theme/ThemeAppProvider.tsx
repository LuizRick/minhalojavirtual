import { ThemeProvider as MuiThemeProvider } from "@mui/material/styles";
import { StyledEngineProvider } from "@mui/styled-engine-sc";
import { useAppThemeContext } from "context/AppThemeContext";
import { ThemeProvider } from "next-themes";
import { ReactNode } from "react";

interface ThemeAppProviderProps {
  children: ReactNode;
  attribute: string | undefined;
}

export default function ThemeAppProvider({
  children,
  attribute,
}: ThemeAppProviderProps) {
  const appContext = useAppThemeContext();
  return (
    <>
      <StyledEngineProvider injectFirst>
        <MuiThemeProvider theme={appContext.theme}>
          <ThemeProvider attribute={attribute}>{children}</ThemeProvider>
        </MuiThemeProvider>
      </StyledEngineProvider>
    </>
  );
}
