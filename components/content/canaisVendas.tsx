import CardIcon from "@/components/blocks/cardicon";
import { useLandingPageContext } from "@/context/LadingPageContext";
import {
  ArrowForward,
  Facebook,
  Google,
  Instagram,
  WhatsApp,
} from "@mui/icons-material";
import { Box, Button, Grid, Paper, Typography } from "@mui/material";

export default function CanaisVendas() {
  const context = useLandingPageContext();
  return (
    <Grid item xs={12} data-aos="fade-right" data-aos-duration="800">
      <Paper className="py-10">
        <Box className="md:w-[793px] md:mx-auto lg:w-[1024px] lg:mx-auto">
          <Typography variant="h2" align="center">
            Canais de vendas.
          </Typography>
          <Typography variant="subtitle1" align="center" color="info">
            Além de seu próprio e-commerce, realizamos a integração com outros
            canais.
          </Typography>
          <Grid container>
            <Grid item xs={12} md={6}>
              <CardIcon
                title="WhatsApp"
                icon={<WhatsApp className="text-green-400 text-4xl" />}
              >
                Converta suas conversas em vendas com as nossas funcionalidades.
              </CardIcon>
            </Grid>
            <Grid item xs={12} md={6}>
              <CardIcon
                title="Instagram Shopping"
                icon={<Instagram className="text-pink-300 text-4xl" />}
              >
                Conecte o seu catálogo ao Instagram Shopping e aumente as suas
                vendas.
              </CardIcon>
            </Grid>
            <Grid item xs={12} md={6}>
              <CardIcon
                title="Facebook"
                icon={<Facebook className="text-blue-700 text-4xl" />}
              >
                Seus produtos sincronizados com o Facebook Shop.
              </CardIcon>
            </Grid>
            <Grid item xs={12} md={6}>
              <CardIcon
                title="Google Shopping"
                icon={<Google className="text-blue-700 text-4xl" />}
              >
                Integração da Loja com o comparador de preços do Google.
              </CardIcon>
            </Grid>
          </Grid>
          <div className="flex items-center justify-center py-10">
            <Button
              color="primary"
              variant="contained"
              endIcon={<ArrowForward />}
              className="text-1xl"
              onClick={() => {
                context.goToZap("Olá, quero saber mais como criar minha loja virtual")
              }}
            >
              Comece sua loja virtual!
            </Button>
          </div>
        </Box>
      </Paper>
    </Grid>
  );
}
