import FeatureCard from "@/components/blocks/featureCard";
import {
  DevicesOther,
  Shop,
  Smartphone,
  SupportAgent
} from "@mui/icons-material";
import { Box, Grid, Typography } from "@mui/material";

export default function MainFeatures() {
  return (
    <Grid item xs={12}>
      <Box className="py-10" >
        <Typography
          component="h2"
          variant="h2"
          className="text-center py-5 text-sky-500"
        >
          Vantagens Nuvemshop
        </Typography>

        <Grid
          className="px-5 md:justify-center md:w-[60%] md:mx-auto"
          container
          gap={3}
          data-aos="fade-up"
        >
          <Grid item xs={12} md={5}>
            <FeatureCard
              icon={<Shop className="dark:text-green-300" />}
              title="Prática e Robusta"
            >
              <div className="md:h-min-[96px]">
                Uma plataforma prática com planos para todos os tipos
                de negócios - micro, pequenos, médias e grandes empresas.
              </div>
            </FeatureCard>
          </Grid>
          <Grid item xs={12} md={5}>
            <FeatureCard
              icon={<DevicesOther className="dark:text-blue-300" />}
              title="Diversos temas"
            >
              <div className="md:h-min-[96px]">
                Há vários temas disponíveis para todos os tipos de negócio.
                Também é possível desenvolver o seu próprio tema.{" "}
              </div>
            </FeatureCard>
          </Grid>
          <Grid item xs={12} md={5}>
            <FeatureCard
              icon={<SupportAgent className="dark:text-amber-500" />}
              title="Suporte"
            >
              A plataforma oferece vários tipos de suporte e treinamento para
              sua empresa, que vão desde consultoria gratuita até paga,
              dependendo do seu plano.
            </FeatureCard>
          </Grid>
          <Grid item xs={12} md={5}>
            <FeatureCard
              icon={<Smartphone className="dark:text-blue-300" />}
              title="Feito para mobile"
            >
              Mais de 70% das visitas vêm de celulares, por isso preparamos o
              layout da sua loja para vender em dispositivos móveis de forma
              amigável.
            </FeatureCard>
          </Grid>
        </Grid>
      </Box>
    </Grid>
  );
}
