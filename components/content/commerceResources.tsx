import FeatureCard from "@/components/blocks/featureCard";
import { useLandingPageContext } from "@/context/LadingPageContext";
import {
  Apps,
  ArrowForward,
  CloudDownload,
  CreditCard,
  Inventory,
  ShoppingBag,
  ViewList,
} from "@mui/icons-material";
import { Box, Button, Grid, Typography } from "@mui/material";

export default function CommerceResources() {
  const context = useLandingPageContext();
  return (
    <Grid item xs={12} data-aos="fade-down" data-aos-duration="1000">
      <Box className="py-10 md:w-[90%] md:mx-auto">
        <Typography
          component="h2"
          variant="h2"
          className="text-center py-5 text-sky-500"
        >
          Recursos que estarão disponíveis em sua loja virtual.
        </Typography>

        <Grid
          className="px-5 md:justify-center md:w-[70%] md:mx-auto"
          container
          gap={3}
        >
          <Grid item xs={12} md={5}>
            <FeatureCard
              icon={<ShoppingBag className="dark:text-orange-300" />}
              title="Quer fazer dropshipping ?"
            >
              <div className="md:h-[96px]">
                A nuvemshop disponibiliza aplicativos pra quem quer vender sem
                estoque.
              </div>
            </FeatureCard>
          </Grid>
          <Grid item xs={12} md={5}>
            <FeatureCard
              icon={<CreditCard className="dark:text-black-300" />}
              title="Ferramentas de pagamento"
            >
              <div className="md:h-[96px]">
                Integração com diversar formas de pagamento e vantagens
                exclusivas pra pagamentos com{" "}
                <a
                  href="https://www.nuvemshop.com.br/nuvem-pay"
                  target="_blank"
                  className="underline"
                  rel="noreferrer"
                >
                  Nuvem Pay
                </a>
                .
              </div>
            </FeatureCard>
          </Grid>
          <Grid item xs={12} md={5}>
            <FeatureCard
              icon={<Inventory className="dark:text-green-300" />}
              title="Gestão de estoque"
            >
              <div className="md:h-[96px]">
                Administre seu estoque a partir de um lugar e deixe de vender
                automaticamente quando o estoque acabar.
              </div>
            </FeatureCard>
          </Grid>
          <Grid item xs={12} md={5}>
            <FeatureCard
              icon={<ViewList className="dark:text-blue-300" />}
              title="Variações de produto"
            >
              <div className="md:h-[96px]">
                Crie produtos e suas variações como tamanho, cor e materiais.
                Cada um com seu próprio SKU, peso e estoque disponível.
              </div>
            </FeatureCard>
          </Grid>
          <Grid item xs={12} md={5}>
            <FeatureCard
              icon={<CloudDownload className="dark:text-amber-500" />}
              title="Produtos digitais"
            >
              Venda cursos, vídeos, livros ou jogos digitais. Nossa plataforma
              está otimizada para produtos físicos (que precisam de envio) e
              digitais.
            </FeatureCard>
          </Grid>
          <Grid item xs={12} md={5}>
            <FeatureCard
              icon={<Apps className="dark:text-blue-300" />}
              title="Loja de aplicativos"
            >
              <div className="md:h-[96px]">
                Loja de aplicativos para gerenciar seu catálogo e vendas
              </div>
            </FeatureCard>
          </Grid>
          <Grid item xs={12}>
            <div
              className="flex item-center justify-center"
              onClick={() => {
                context.goToZap(
                  "Olá, quero saber mais como criar minha loja virtual"
                );
              }}
            >
              <Button variant="contained" endIcon={<ArrowForward />}>
                Quero minha Loja!
              </Button>
            </div>
          </Grid>
        </Grid>
      </Box>
    </Grid>
  );
}
