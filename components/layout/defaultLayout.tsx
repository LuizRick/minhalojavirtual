import {
  WhatsApp
} from "@mui/icons-material";
import {
  AppBar,
  Drawer,
  IconButton,
  Toolbar,
  Typography
} from "@mui/material";
import { useAppThemeContext } from "context/AppThemeContext";
import { useTheme } from "next-themes";
import Head from "next/head";
import { ReactNode, useEffect, useState } from "react";
import DrawerList from "../menu/drawerlist";

interface LayoutProps {
  children: ReactNode;
}

export default function DefaultLayout({ children }: LayoutProps) {
  const { theme, setTheme } = useTheme();
  const themea = useAppThemeContext();
  const [drawer, setDrawer] = useState(false);

  useEffect(() => {
    const themeChoose = localStorage.getItem("themeChoose") || "light";
    if (themea.mode != themeChoose) {
      themea.colorMode.toggleColorMode();
    }
  });

  const toogleDrawer =
    (open: boolean) => (event: KeyboardEvent | MouseEvent) => {
      if (
        event.type === "keydown" &&
        ((event as KeyboardEvent).key === "Tab" ||
          (event as KeyboardEvent).key === "Shift")
      ) {
        return;
      }

      setDrawer(open);
    };

  function appBarLabel(label: string, theme?: string) {
    return (
      <Toolbar color="secondary">
        {/*<IconButton
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
            onClick={() => setDrawer(true)}
          >
            <Menu />
      </IconButton>*/}
        <Typography
          variant="h6"
          noWrap
          component="div"
          sx={{ flexGrow: 1 }}
          className="font-bold"
        >
          {label}
        </Typography>
        <IconButton
          color="primary"
          onClick={() => {
            window.open(
              process.env.NEXT_PUBLIC_WHATSAPP_URL +
                "?text=" +
                encodeURIComponent(
                  "Olá vim pela pagina de vendas. Quero criar minha Loja com nuvem shop!!!"
                )
            );
          }}
        >
          <WhatsApp className="text-4xl text-green-400" />
        </IconButton>
      </Toolbar>
    );
  }

  useEffect(() => {
    setTheme(themea.mode as any);
  });
  return (
    <>
      <Head>
        <link rel="icon" href="/favicon.ico" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1 maximum-scale=1.0, user-scalable=no"
        />
      </Head>
      <AppBar position="static" color="primary">
        {appBarLabel("Parceiro Nuvemshop", theme)}
      </AppBar>
      <Drawer open={drawer} onClose={toogleDrawer(false)}>
        <DrawerList></DrawerList>
      </Drawer>
      <main>{children}</main>
      <footer></footer>
    </>
  );
}
