import {
  Box,
  Card,
  CardContent,
  CardMedia,
  Typography
} from "@mui/material";
import * as React from "react";

export default function CardMediaHorizontal({
  image,
  title,
  children,
}: {
  image: string;
  title?: string;
  children?: React.ReactNode;
}) {
  return (
    <Card sx={{ display: "flex" }}>
      <CardMedia component="img" sx={{ width: 151 }} image={image} className="bg-white" 
        alt={'image - ' + title}/>
      <Box sx={{ display: "flex", flexDirection: "column" }}>
        <CardContent className="flex flex-wrap justify-center items-center w-auto">
          <Typography component="div" variant="h5" className="w-full text-center">
            {title}
          </Typography>
          {children}
        </CardContent>
      </Box>
    </Card>
  );
}
