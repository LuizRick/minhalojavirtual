import { Instagram, LinkedIn } from "@mui/icons-material";
import { IconButton, Paper, Typography } from "@mui/material";
import Link from "next/link";

export default function PaperTopDescription({ styles }: { styles: any }) {
  return (
    <Paper className="bg-slate-100 dark:bg-slate-900" elevation={0}>
      <div className={styles.content}>
        <Typography
          variant="h5"
          className="px-5 py-5 font-bold text-center leading-8"
        >
          Olá, meu nome é Luiz e sou um dos vários parceiros da Nuvemshop, uma
          das maiores plataformas de e-commerce da América Latina 😎.
        </Typography>
        <Typography
          variant="h5"
          className="px-5 py-5 font-bold text-center leading-8"
        >
          A Nuvemshop oferece diversas vantagens para quem quer começar seu
          negócio online. Comece gratuitamente (verifique se atende às suas
          necessidades) ou então por um preço acessível, dependendo do tamanho
          da sua empresa. 🛒
        </Typography>
        <Typography
          variant="h5"
          className="px-5 py-5 font-bold text-center leading-8"
        >
          Caso esteja curioso, veja esta&nbsp;
          <Link
            href="https://demoluizhenrique.lojavirtualnuvem.com.br/"
            className="underline"
            target="_blank"
          >
            loja demonstração{" "}
          </Link>
          que preparei. A plataforma da Nuvemshop oferece planos que atendem ao
          tamanho do seu negócio. 🧑‍💻
        </Typography>
        <Typography
          variant="h5"
          className="px-5 py-5 font-bold text-center leading-8"
        >
          Para conteúdos e informações pessoais acesse minhas redes
        </Typography>
        <div className="flex justify-center">
          <div>
            <IconButton
              size="large"
              onClick={() => {
                window.open(
                  "https://www.linkedin.com/in/luiz-henrique-monteiro-silva-lima/",
                  "_blank"
                );
              }}
            >
              <LinkedIn className="text-6xl text-blue-700" />
            </IconButton>
          </div>
          <div>
            <IconButton
              size="large"
              onClick={() => {
                window.open(
                  "https://www.instagram.com/minhalojaecommerceoficial/",
                  "_blank"
                );
              }}
            >
              <Instagram className="text-6xl text-pink-400" />
            </IconButton>
          </div>
        </div>
      </div>
    </Paper>
  );
}
