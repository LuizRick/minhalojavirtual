import {
  Avatar,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Typography,
} from "@mui/material";
import * as React from "react";

export default function CardIcon({
  icon,
  title,
  children,
}: {
  icon: React.ReactNode;
  title?: string;
  children?: React.ReactNode;
}) {
  return (
    <List sx={{ width: "100%", maxWidth: 560, paddingLeft: 1, paddingTop: 1 }}>
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Avatar sx={{ backgroundColor: "transparent" }}>{icon}</Avatar>
        </ListItemAvatar>
        <ListItemText
          primary={<Typography variant="h5">{title}</Typography>}
          secondary={<Typography variant="body2">{children}</Typography>}
        />
      </ListItem>
    </List>
  );
}
