import Image from "next/image";
import { useState } from "react";
import Lightbox from "react-image-lightbox";
import "react-image-lightbox/style.css";
import Slider, { Settings } from "react-slick";

export interface CarouselProps {
  settings?: Settings;
  images: string[];
}

export function CarouselLightbox({ settings, images }: CarouselProps) {
  const defaultSettings: Settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
  };
  const [isOpen, setIsOpen] = useState(false);
  const [photoIndex, setPhotoIndex] = useState(0);
  const [mainSrc, setMainSrc] = useState("");
  return (
    <div>
      <Slider {...defaultSettings}>
        {images.map((item, index) => (
          <div key={index} className="hover:cursor-pointer">
            <Image
              src={item}
              alt="carrosel image"
              width={300}
              height={300}
              className="filterWhiteTranp"
              onClick={() => {
                setIsOpen(true);
                setPhotoIndex(index);
                setMainSrc(images[index]);
              }}
            />
          </div>
        ))}
      </Slider>
      {isOpen && (
        <Lightbox
          mainSrc={mainSrc}
          nextSrc={images[(photoIndex + 1) % images.length]}
          prevSrc={images[(photoIndex + images.length - 1) % images.length]}
          onCloseRequest={() => {
            setIsOpen(false)
          }}
          onMoveNextRequest={() =>
            setPhotoIndex((photoIndex + 1) % images.length)
          }
          onMovePrevRequest={() =>
            setPhotoIndex((photoIndex + images.length - 1) % images.length)
          }
        />
      )}
    </div>
  );
}
