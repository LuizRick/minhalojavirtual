import { LoadingButton } from "@mui/lab";
import { Grid, TextField, Typography } from "@mui/material";
import Link from "next/link";
import { useState } from "react";

export interface State {
  nome: string;
  email: string;
  validEmail: boolean;
  validName: boolean;
}

interface FeedBackMsg {
  errorMsg?: string;
  nameErroMsg?: string;
}

interface ResponsiveNewsLetterProsp {
  onSubscribe?: (nome: string, email: string) => void;
  sendProgress: boolean;
}

export default function ResponsiveNewsLetter(props: ResponsiveNewsLetterProsp) {
  const [values, setValues] = useState<State>({
    email: "",
    nome: "",
    validEmail: true,
    validName: true,
  });

  const [msgs, setMsgs] = useState<FeedBackMsg>({
    errorMsg: "",
    nameErroMsg: "",
  });

  const handleChange =
    (prop: keyof State) => (event: React.ChangeEvent<HTMLInputElement>) => {
      setValues({ ...values, [prop]: event.target.value });
    };

  const validateEmail = () => {
    const isValid = isValidEmail(values.email);
    setValues({
      ...values,
      validEmail: isValid,
    });

    setMsgs({
      errorMsg: isValid ? "" : "Por favor preencha o email corretamente!",
    });
  };

  const validateNome = () => {
    const isValid = isValidNome(values.nome);
    setValues({
      ...values,
      validName: isValid,
    });

    setMsgs({
      nameErroMsg: isValid ? "" : "Por favor preencha um nome!",
    });
  };

  const handleEmail = async () => {
    if (isValidEmail(values.email) && isValidNome(values.nome)) {
      fetch("/api/email", {
        method: "POST",
        body: JSON.stringify({
          email: "",
          nome: "",
        }),
      });
    }
  };

  return (
    <Grid container gap={1}>
      <Grid item xs={12}>
        <Typography variant="h3">
          Se inscreva pra receber mais informações!
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <TextField
          label="Nome"
          fullWidth
          value={values.nome}
          error={!values.validName}
          onChange={handleChange("nome")}
          required
          onBlur={validateNome}
          helperText={msgs.nameErroMsg}
        ></TextField>
      </Grid>
      <Grid item xs={12}>
        <TextField
          label="Email"
          fullWidth
          value={values.email}
          onChange={handleChange("email")}
          error={!values.validEmail}
          onBlur={validateEmail}
          helperText={msgs.errorMsg}
          required
        ></TextField>
      </Grid>
      <Grid item xs={12}>
        <LoadingButton
          loading={props.sendProgress}
          onClick={() => {
            if (isValidEmail(values.email) && isValidNome(values.nome)) {
              props.onSubscribe?.call(null, values.nome, values.email);
            }
          }}
          variant="contained"
          id="btn_newsubscribe"
        >
          <span>inscrever-se</span>
        </LoadingButton>
      </Grid>
      <Grid item xs={12}>
        <Typography variant="caption">
          Ao clicar em &quot;inscrever-se&ldquo;, você aceita receber nossa newsletter e
          novas ofertas via email.
        </Typography>
        <Link href="http://eepurl.com/iofrKI" className="underline block" target="_blank">Powered by Mailchimp</Link>
      </Grid>
    </Grid>
  );
}

function isValidEmail(email: string) {
  const re =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email.toLowerCase());
}

function isValidNome(nome: string) {
  const re = /^[a-z ,.'-]+$/i;
  return re.test(nome.toLowerCase());
}
