import { Box } from "@mui/system";


interface HorizontalCardIconProps{
    icon?: React.ReactNode;
    title?: string;
    children?: React.ReactNode;
}

export default function HorizontalCardIcon( { icon, title, children } : HorizontalCardIconProps){
    return (
        <Box>
            <div className="flex items-center justify-center">
                {icon}
            </div>
            <div className="flex items-center justify-center">
                <h3>{title}</h3>
            </div>
            <div className="flex items-center justify-center text-center">
                <p className="dark:text-slate-300">{children}</p>
            </div>
        </Box>
    )
}