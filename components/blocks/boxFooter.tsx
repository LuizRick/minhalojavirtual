import { Instagram, LinkedIn, Twitter } from "@mui/icons-material";
import { Box, IconButton } from "@mui/material";
import Image from "next/image";


export default function BoxFooter(){
    return (
        <Box className="p-3">
        <div className="flex items-center w-full">
          <div>
            <IconButton
              onClick={() => {
                window.open("https://twitter.com/luizhenriquemsl", "_blank");
              }}
            >
              <Twitter />
            </IconButton>
          </div>
          <div>
            <IconButton
              onClick={() => {
                window.open(
                  "https://www.linkedin.com/in/luiz-henrique-monteiro-silva-lima/",
                  "_blank"
                );
              }}
            >
              <LinkedIn />
            </IconButton>
          </div>
          <div>
            <IconButton
              onClick={() => {
                window.open(
                  "https://www.instagram.com/minhalojaecommerceoficial/",
                  "_blank"
                );
              }}
            >
              <Instagram />
            </IconButton>
          </div>
          <div className="flex-grow"></div>
          <div className="relative w-[450px] h-[120px]">
            <Image
              src={"/images/selo-nuvemshop-parceiro-desenv.png"}
              fill
              alt="Logo Parceiro Desenvolvimento"
            />
          </div>
        </div>
      </Box>
    )
}