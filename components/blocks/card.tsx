import React from 'react';

export interface Props {
  title: string;
  description: string;
  buttonText: string;
  onButtonClick: () => void;
  imageUrl: string;
}

const Card: React.FC<Props> = ({ title, description, buttonText, onButtonClick, imageUrl }) => {
  return (
    <div className="bg-white rounded-lg shadow-lg overflow-hidden">
      <img src={imageUrl} alt={title} className="w-full h-48 object-cover" />
      <div className="p-6">
        <h3 className="text-lg font-medium">{title}</h3>
        <p className="text-gray-700 mt-2">{description}</p>
        <button 
          className="bg-indigo-500 text-white py-2 px-4 rounded-lg mt-4 hover:bg-indigo-600"
          onClick={onButtonClick}>
            {buttonText}
        </button>
      </div>
    </div>
  );
}

export default Card;
