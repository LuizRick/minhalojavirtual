import { Avatar, Card, CardContent, CardHeader, useTheme } from "@mui/material";
import Image from "next/image";
import styles from 'styles/components/featureCard.module.css';

interface FeatureCardProps {
  icon?: React.ReactNode;
  title?: string;
  children?: React.ReactNode;
}

export default function FeatureCard({
  icon,
  title,
  children,
}: FeatureCardProps) {
  const theme = useTheme();
  return (
    <div className={styles.card}>
      <div className={styles.ribbon}>
        <Image src='/ribbon.png' width={38} height={70} alt="ribbon"/>
      </div>
      <Card
        sx={{
          maxWidth: 410,
          borderRadius: 3,
        }}
        elevation={0}
      >
        <CardHeader
          avatar={
            <Avatar
              sx={{
                bgcolor: theme.palette.primary.main,
                width: 56,
                height: 56,
              }}
            >
              {icon}
            </Avatar>
          }
        />
        <CardContent>
          <div className="lg:w-[300px]">
            <h3 className="m-0 mb-5">{title}</h3>
            <div className="leading-6">{children}</div>
          </div>
        </CardContent>
      </Card>
    </div>
  );
}
