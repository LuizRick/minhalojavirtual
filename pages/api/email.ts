// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";


export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const auth = Buffer.from(
    `${process.env.MAILCHIP_MAIL_ACCOUNT}:${process.env.MAILCHIP_KEY_API}`
  ).toString("base64");

  console.log(auth + " - key")

  if (req.method === "POST") {
    const listId = "801d8daf7e";
    if(validateNomeEmail(req.body.email, req.body.nome)){
        const response = await fetch(`https://us11.api.mailchimp.com/3.0/lists/${listId}/members`,{
            method: "POST",
            headers: {
              Authorization: "Basic " + auth,
            },
            body: JSON.stringify({
                email_address: req.body.email,
                status: 'subscribed',
                merge_fields:{
                    FNAME: req.body.nome
                }
            })
        });
        const body = await response.json();
        res.status(response.status).json({
          detail: body.title,
        } as any);
    }else{
        res.status(400).json({detail: 'Campos Invalidos'} as any);
    }
    
  }
  
  if (req.method === "GET") {
    const response = await fetch("https://us11.api.mailchimp.com/3.0/ping", {
      method: "GET",
      headers: {
        Authorization: "Basic " + auth,
      },
    }).then((r) => r.json());
    res.status(200).json(response);
  }
}

function validateNomeEmail(email: string, nome: string) {
  const reEmail =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const reName = /^[a-z ,.'-]+$/i;
    return reEmail.test(email) && reName.test(nome);
}
