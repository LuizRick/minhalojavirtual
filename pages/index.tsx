import SlidingDiagonals from "@/components/backgrounds/slidingDiagonals";
import BoxFooter from "@/components/blocks/boxFooter";
import CardImage from "@/components/blocks/cardMedia";
import PaperTopDescription from "@/components/blocks/paperTopDescription";
import ResponsiveNewsLetter from "@/components/blocks/responsiveNewsLetter";
import CanaisVendas from "@/components/content/canaisVendas";
import CommerceResources from "@/components/content/commerceResources";
import MainFeatures from "@/components/content/mainFeatures";
import LandingPageContextMode, {
  goToZap,
  LandingpageContext,
} from "@/context/LadingPageContext";
import styles from "@/styles/Home.module.css";
import {
  ArrowOutward,
  Discount,
  LocalShipping,
  WhatsApp
} from "@mui/icons-material";
import {
  Alert,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Collapse,
  Grid,
  Paper,
  Typography
} from "@mui/material";
import "aos/dist/aos.css";
import PaimentIllustration from "assets/Online_payment_Isometric.svg";
import Head from "next/head";
import { useState } from "react";

interface ComponentState {
  alertError: boolean;
  alertErrorMessage: string;
  subscribeEvent: boolean;
  typeAlert?: "success" | "info" | "warning" | "error";
}

export default function Home() {
  const sharedState = {
    goToZap,
  } as LandingpageContext;

  const [cmpState, setCmpState] = useState<ComponentState>({
    alertError: false,
    alertErrorMessage: "",
    subscribeEvent: false,
  });

  return (
    <LandingPageContextMode.Provider value={sharedState}>
      <Head>
        <title>Parceiro Nuvemshop - Crie sua loja virtual em poucos passos.</title>
        <meta
          name="description"
          content="Nuvemshop partner - Crie sua loja virtual em poucos passos - comece gratuitamente"
        />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className="absolute-background">
        <div></div>
      </div>
      <Grid container>
        <Grid item xs={12}>
          <Paper component="header">
            <div className={styles.content}>
              <Typography variant="h1" className="px-2" align="center">
                <Typography
                  variant="inherit"
                  component="span"
                  color="primary"
                  className="font-bold"
                >
                  Crie sua loja virtual&nbsp;
                </Typography>
                em poucos passos. 🚀
              </Typography>
              <div className="flex flex-wrap justify-center p-10">
                <Button
                  color="primary"
                  variant="contained"
                  startIcon={<WhatsApp className="text-4xl"/>}
                  onClick={() =>
                    sharedState.goToZap(
                      "Olá Luiz, Quero marcar uma apresentação comercial pra entender melhor"
                    )
                  }
                >
                   Marcar Apresentação comercial!
                </Button>
              </div>
            </div>
          </Paper>
          <PaperTopDescription styles={styles}></PaperTopDescription>
        </Grid>
        <Grid item xs={12}>
          <Paper
            className="w-[100%] md:w-[560px] md:mx-auto mt-12 px-1"
            elevation={0}
          >
            <Card data-aos="fade-left" elevation={0}>
              <CardHeader
                title="Comece Investindo pouco dinheiro 📈"
                avatar={<Discount />}
              />
              <CardContent>
                <Typography variant="h6">
                  Aproveite os descontos e a oportunidade de ter sua loja com
                  preços a partir de{" "}
                  <span className="text-green-700 dark:text-green-500">
                    R$ 59/mês
                  </span>* {". "}
                  Ou, se preferir, faça um{" "}
                  <span className="text-green-600 dark:text-green-300">
                    teste grátis de 30 dias.
                  </span>
                  O plano vem com minha consultoria de como usar a plataforma.
                </Typography>
                <Typography variant="subtitle2">
                  * os valores podem mudar a qualquer momento.
                </Typography>
              </CardContent>
              <CardActions>
                <Button
                  variant="contained"
                  onClick={() =>
                    sharedState.goToZap("Olá, quero fazer um teste gratis")
                  }
                >
                  Quero testar!
                </Button>
              </CardActions>
            </Card>
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <Paper
            className="w-[100%] md:w-[560px] md:mx-auto mt-12 px-1"
            elevation={0}
          >
            <iframe
              src="https://www.youtube.com/embed/Ls5gsIMUfgg"
              title="YouTube video player"
              frameBorder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
              allowFullScreen
              className="w-[100%] md:w-[560px] mx-auto h-[320px]"
              data-aos="fade-up"
            ></iframe>
          </Paper>
          <div className="flex flex-wrap justify-center mt-10">
            <Button
              variant="contained"
              startIcon={<ArrowOutward />}
              onClick={() => {
                window.open(
                  "https://demoluizhenrique.lojavirtualnuvem.com.br/",
                  "_blank"
                );
              }}
            >
              Ver Demonstração
            </Button>
          </div>
        </Grid>
      </Grid>
      <Grid container>
        <MainFeatures></MainFeatures>
      </Grid>
      <Grid container>
        <CanaisVendas></CanaisVendas>
      </Grid>
      <Grid container>
        <CommerceResources></CommerceResources>
      </Grid>
      <SlidingDiagonals className="pt-10 pb-10 overflow-hidden">
        <Grid container gap={1} className="md:mx-auto">
          <Grid item xs={12} sx={{ paddingX: 1 }}>
            <Paper className="p-10 flex items-center justify-center">
              <LocalShipping className="text-6xl float-left mr-10" />
              <Typography variant="h3" align="center">
                Envios
              </Typography>
            </Paper>
          </Grid>
        </Grid>

        <Grid
          container
          gap={1}
          className="mt-2"
          justifyContent="center"
          data-aos="zoom-in"
        >
          <Grid item xs={12} md={5} sx={{ paddingX: 1 }}>
            <CardImage image="/images/correios.png" title="Correios">
              Faça envios para todo o país com ou sem contrato, usando todas as
              modalidades dos Correios.
            </CardImage>
          </Grid>
          <Grid item xs={12} md={5} sx={{ paddingX: 1 }}>
            <CardImage image="/images/melhor-envio.png" title="Melhor envio">
              Frete competitivo e rastreio inteligente em diversas
              transportadoras.
            </CardImage>
          </Grid>

          <Grid item xs={12} md={5} sx={{ paddingX: 1 }}>
            <CardImage image="/images/frenet.png" title="Frenet">
              Integre seu e-comme rce com mais de 245 transportadoras em todo
              Brasil.
            </CardImage>
          </Grid>

          <Grid item xs={12} md={5} sx={{ paddingX: 1 }}>
            <CardImage image="/images/kangu.png" title="Kangu">
              Fretes super competitivos e uma rede de pontos para postagem.
            </CardImage>
          </Grid>
        </Grid>
      </SlidingDiagonals>
      <Grid
        container
        justifyContent="center"
        data-aos="zoom-out"
        data-aos-duration="1000"
      >
        <Grid item xs={12} sx={{ paddingX: 1 }}>
          <Paper elevation={0} className="py-3">
            <Typography variant="h3" align="center">
              Formas de pagamento
            </Typography>
            <Typography align="center">
              Fornecemos uma série de serviços de pagamento seguros 🔒 para sua
              loja virtual.
            </Typography>
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <div className="w-[100%] md:w-[20%] mx-auto">
            <PaimentIllustration />
          </div>
        </Grid>
        <Grid item xs={12} md={5}>
          <div className="p-1">
            <CardImage title="Mercado Pago" image="/images/mercado-pago.png">
              Aceite diferentes meios de pagamento, como boleto bancário, cartão
              de crédito e transferência bancária.
            </CardImage>
          </div>
        </Grid>
        <Grid item xs={12} md={5}>
          <div className="p-1">
            <CardImage title="PagSeguro" image="/images/pagseguro.png">
              Receba pagamentos via cartões de crédito, boleto e transferências
              bancárias online.
            </CardImage>
          </div>
        </Grid>
        <Grid item xs={12} md={5}>
          <div className="p-1">
            <CardImage title="PayPal" image="/images/paypal.png">
              Receba pagamentos com cartões internacionais de compras realizadas
              fora do país.
            </CardImage>
          </div>
        </Grid>
        <Grid item xs={12} md={12}>
          <div className="flex flex-wrap items-center justify-center">
            <Typography
              className="uppercase  font-bold w-[100%] text-center py-2"
              component="div"
            >
              E muitas outras formas de pagamento
            </Typography>
            <Button
              variant="contained"
              onClick={() =>
                sharedState.goToZap(
                  "Gostei das formas de pagamento, quero saber mais sobre criar minha loja."
                )
              }
            >
              Quero criar minha loja!
            </Button>
          </div>
        </Grid>
      </Grid>
      <Paper className="text-center mt-10">
        <Grid container sx={{ paddingY: 1 }} data-aos="flip-up">
          <Grid item xs={12}>
            <Typography variant="h2">
              Para negócios de todos os tipos e tamanhos
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography
              variant="subtitle1"
              className="mb-10 mt-1 font-bold px-1 py-2"
            >
              Para quem está começando, para pequenas e médias empresas e para
              grandes marcas. Temos a opção ideal para o seu negócio digital ou
              físico.
            </Typography>
            <Button
              variant="contained"
              className="text-1xl"
              endIcon={<WhatsApp />}
              onClick={() =>
                sharedState.goToZap(
                  "Quero saber mais sobre a plataforma de ecommerce"
                )
              }
            >
              Gostei quero saber mais
            </Button>
          </Grid>
          <Grid item xs={12} className="py-2 px-1">
            <div className="w-[100%] md:w-[760px] mx-auto">
              <ResponsiveNewsLetter
                sendProgress={cmpState.subscribeEvent}
                onSubscribe={(nome, email) => {
                  window.dataLayer.push({
                    event: 'NewsLetterSubscription'
                  });
                  setCmpState({
                    ...cmpState,
                    subscribeEvent: true,
                    alertError: false,
                  });

                  fetch(`${process.env.NEXT_PUBLIC_BACKEND}/email-marketing`, {
                    method: "POST",
                    headers: {
                      "Content-Type": "application/json",
                      "X-Parse-Application-Id": "SZpAdCxfP6Vj9y5aCoyLurVzEgwlTFY2TpVMU57u",
                      "X-Parse-REST-API-Key": "hUVbBUtrho1qBinkBADMHZGIGELR8hwGlyiR3fSr"
                    },
                    body: JSON.stringify({
                      nome: nome,
                      email: email,
                    }),
                  })
                    .then(async (r) => {
                      const ok = r.status === 200;
                      const data = await r.json();
                      setCmpState({
                        alertError: true,
                        alertErrorMessage: ok
                          ? "Obrigado pela inscrição"
                          : "Não foi possível se inscrever." + `[${data.error}]`,
                        subscribeEvent: false,
                        typeAlert: ok ? "success" : "error",
                      });
                    })
                    .catch((err) => {
                      console.log(err);
                      setCmpState({
                        alertError: true,
                        alertErrorMessage:
                          "Não possível fazer a sua incrição se o erro persistir tente novamente mais tarde!",
                        subscribeEvent: false,
                        typeAlert: "error",
                      });
                    });
                }}
              ></ResponsiveNewsLetter>
            </div>
          </Grid>
          <Grid item xs={12}>
            <Collapse in={cmpState.alertError}>
              <Alert severity={cmpState.typeAlert}>
                {cmpState.alertErrorMessage}
              </Alert>
            </Collapse>
          </Grid>
        </Grid>
      </Paper>
      <BoxFooter></BoxFooter>
    </LandingPageContextMode.Provider>
  );
}

declare global{
  interface Window{
    dataLayer: any;
  }
}