import "@/styles/globals.css";
import type { AppProps } from "next/app";
import { useEffect } from "react";
import AOS from "aos";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import ThemeAppProvider from "@/components/theme/ThemeAppProvider";
import { AppWrapper } from "@/context/AppThemeContext";
import DefaultLayout from "@/components/layout/defaultLayout";
import Tagmanager from "react-gtm-module";
import { getGtmId } from "@/lib/utils";

export default function App({ Component, pageProps }: AppProps) {
  useEffect(() => {
    AOS.init({
      easing: "ease-in-out",
      once: false,
      offset: 50,
    });

    Tagmanager.initialize({
      gtmId: getGtmId() as string,
    });

    document.documentElement.setAttribute("lang", "pt-BR");
  }, []);

  return (
    <AppWrapper>
      <ThemeAppProvider attribute="class">
        <DefaultLayout>
          <Component {...pageProps} />
        </DefaultLayout>
      </ThemeAppProvider>
    </AppWrapper>
  );
}
