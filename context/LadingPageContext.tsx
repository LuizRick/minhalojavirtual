import React from "react";

export interface LandingpageContext {
  goToZap: (msg: string) => void;
}

export const goToZap = (msg: string) => {
  window.open(
    process.env.NEXT_PUBLIC_WHATSAPP_URL + "?text=" + encodeURIComponent(msg)
  );
};

const LandingPageContextMode = React.createContext<LandingpageContext>({
  goToZap,
});

export default LandingPageContextMode;

export function useLandingPageContext() {
  return React.useContext(LandingPageContextMode);
}
