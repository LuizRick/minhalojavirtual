import { PaletteMode, responsiveFontSizes, ThemeOptions } from "@mui/material";
import { createTheme, Theme } from "@mui/material/styles";
import { StyledEngineProvider } from "@mui/styled-engine-sc";
import * as React from "react";

interface AppThemeContext {
  colorMode: {
    toggleColorMode: () => void;
  };
  theme: Partial<Theme> | ((outerTheme: Theme) => Theme);
  mode?: "light" | "dark";
}

const getDesignTokens = (mode: PaletteMode) =>
  ({
    palette: {
      mode,
      ...(mode === "light"
        ? {
            primary: {
              main: "#5670bd",
            },
            secondary: {
              main: "#191970",
            },
            background: {
              paper: "#fffafa",
            },
          }
        : {
            primary: {
              main: "#1f6f7b",
            },
            secondary: {
              main: "#8691b7",
            },
            error: {
              main: "#e66f6f",
            },
            text: {
              primary: "#e0e0e0",
            },
            warning: {
              main: "#ff9100",
            },
          }),
    },
    typography: {
      ...(mode === "dark"
        ? {
            poster: {
              color: "slategray",
            },
            contrast: {
              color: "Highlight",
              display: "block",
            },
            frontCover: {
              color: "white",
            },
          }
        : {
            poster: {
              color: "green",
            },
            frontCover: {
              color: "white",
            },
          }),
    },
  } as ThemeOptions);

const AppThemeContextMode = React.createContext<AppThemeContext>({
  colorMode: {
    toggleColorMode: () => {},
  },
  theme: createTheme(),
});

export function AppWrapper({ children }: { children: React.ReactNode }) {
  const [mode, setMode] = React.useState<"light" | "dark">("light");

  const colorMode = React.useMemo(
    () => ({
      toggleColorMode: () => {
        setMode((prevMode) => {
          const mode = prevMode === "light" ? "dark" : "light";
          return mode;
        });
      },
    }),
    []
  );

  const themeMui = React.useMemo(
    () => responsiveFontSizes(createTheme(getDesignTokens(mode))),
    [mode]
  );

  let sharedState: AppThemeContext = {
    colorMode,
    theme: themeMui,
    mode,
  };

  return (
    <StyledEngineProvider injectFirst>
      <AppThemeContextMode.Provider value={sharedState}>
        {children}
      </AppThemeContextMode.Provider>
    </StyledEngineProvider>
  );
}

declare module "@mui/material/styles" {
  interface TypographyVariants {
    poster: React.CSSProperties;
    contrast: React.CSSProperties;
    frontCover: React.CSSProperties;
  }

  interface TypographyVariantsOptions {
    poster?: React.CSSProperties;
    contrast?: React.CSSProperties;
    frontCover?: React.CSSProperties;
  }
}

declare module "@mui/material/Typography" {
  interface TypographyPropsVariantOverrides {
    poster: true;
    contrast: true;
    frontCover: true;
  }
}

export function useAppThemeContext() {
  return React.useContext(AppThemeContextMode);
}
