const path = require('path')
/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  env:{
    STRAPI_API_GRAPHQL: process.env.STRAPI_GRAPHQL_ENDPOINT || "http://127.0.0.1:1337",
    NEXT_PUBLIC_STRAPI_URL: process.env.STRAPI_URL || "http://127.0.0.1:1337",
    NEXT_PUBLIC_WHATSAPP_URL: process.env.WHATSAPP_URL || "https://wa.me/5511944445555",
    NEXT_PUBLIC_GTM_ID: process.env.GTM_ID || "GTM-KLHCBBZ",
    MAILCHIP_KEY_API: process.env.API_KEY_MAILCHIP,
    MAILCHIP_MAIL_ACCOUNT: process.env.EMAIL_ACCOUNT,
    NEXT_PUBLIC_BACKEND: 'https://parseapi.back4app.com/functions'
  },
  images:{
    domains:['127.0.0.1']
  },
  sassOptions: {
    includePaths: [path.join(__dirname, "styles")],
  },
  webpack(config){
    config.module.rules.push({
      test: /\.svg$/i,
      issuer: /\.[jt]sx?$/,
      use: ['@svgr/webpack'],
    });
    return config;
  }
}

module.exports = nextConfig
