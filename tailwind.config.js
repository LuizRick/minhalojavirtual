/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  darkMode: "class",
  theme: {
    extend: {
      colors: {
        "slate-dark": "rgb(26, 33, 56)",
      },
    },
  },
  corePlugins:{
    preflight: false
  },
  theme: {
    extend: {},
  },
  plugins: [],
  important: true
}
